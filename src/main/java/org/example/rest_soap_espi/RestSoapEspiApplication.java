package org.example.rest_soap_espi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestSoapEspiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestSoapEspiApplication.class, args);
    }

}
